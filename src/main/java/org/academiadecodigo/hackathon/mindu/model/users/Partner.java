package org.academiadecodigo.hackathon.mindu.model.users;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "partner")
public class Partner extends AbstractUser {

    // professional body (must be a POJO)
    @OneToMany(fetch = FetchType.EAGER,
    mappedBy = "partner")
    private List<User> users = new ArrayList<>();

    private String certificate;
    private String location;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
