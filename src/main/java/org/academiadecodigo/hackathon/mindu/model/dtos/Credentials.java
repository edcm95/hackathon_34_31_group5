package org.academiadecodigo.hackathon.mindu.model.dtos;

public class Credentials {

    private String email;
    private String authentication;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAuthentication() {
        return authentication;
    }

    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }
}
