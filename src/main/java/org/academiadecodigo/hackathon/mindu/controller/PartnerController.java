package org.academiadecodigo.hackathon.mindu.controller;

import org.academiadecodigo.hackathon.mindu.errorhandling.exception.MindUException;
import org.academiadecodigo.hackathon.mindu.model.users.Partner;
import org.academiadecodigo.hackathon.mindu.services.users.PartnerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/partner")
public class PartnerController {

    private PartnerServiceImpl paramService;

    @Autowired
    public void setParamService(PartnerServiceImpl paramService) {
        this.paramService = paramService;
    }

    @RequestMapping(method = RequestMethod.GET, path = {"/", ""})
    public ResponseEntity<List<Partner>> listPartners() {
        return new ResponseEntity<>(paramService.list(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/email/{email}")
    public ResponseEntity<Partner> getPartnerByEmail(@PathVariable String email) throws MindUException {
        return new ResponseEntity<>(paramService.findByEmail(email), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = {"/", ""})
    public ResponseEntity<HttpHeaders> addPartner(@RequestBody Partner partner, UriComponentsBuilder uriComponentsBuilder) {
        Partner savedPartner = paramService.save(partner);

        UriComponents uriComponents = uriComponentsBuilder.path("/api/partner/" + savedPartner.getId()).build();

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(uriComponents.toUri());

        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }
}
