package org.academiadecodigo.hackathon.mindu.controller;

import org.academiadecodigo.hackathon.mindu.errorhandling.exception.MindUException;
import org.academiadecodigo.hackathon.mindu.model.users.Partner;
import org.academiadecodigo.hackathon.mindu.model.users.User;
import org.academiadecodigo.hackathon.mindu.services.ParamService;
import org.academiadecodigo.hackathon.mindu.services.users.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/user")
public class UserController {

    private UserServiceImpl paramService;

    @Autowired
    public void setUserService(UserServiceImpl paramService) {
        this.paramService = paramService;
    }

    @RequestMapping(method = RequestMethod.GET, path = {"/", ""})
    public ResponseEntity<List<User>> listUsers() {
        return new ResponseEntity<>(paramService.list(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/email/{email}")
    public ResponseEntity<User> getUserByEmail(@PathVariable String email) throws MindUException {
        return new ResponseEntity<>(paramService.findByEmail(email), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, path = {"/", ""})
    public ResponseEntity<HttpHeaders> addUser(@RequestBody User user, UriComponentsBuilder uriComponentsBuilder) {

        User savedUser = paramService.save(user);

        UriComponents uriComponents = uriComponentsBuilder.path("/api/user/" + savedUser.getId()).build();

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(uriComponents.toUri());

        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }
}
