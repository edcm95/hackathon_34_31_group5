package org.academiadecodigo.hackathon.mindu.errorhandling.exception;

public class MindUException extends Exception {

    public MindUException(String message){
        super(message);
    }
}
