package org.academiadecodigo.hackathon.mindu.dao.userdaos;

import org.academiadecodigo.hackathon.mindu.model.users.Partner;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class JPAProfessionalDao implements UserDao<Partner> {

    private EntityManager em;

    @PersistenceContext
    public void setEm(EntityManager em) {
        this.em = em;
    }

    @Override
    public List<Partner> findAll() {
        CriteriaQuery<Partner> criteriaQuery = em.getCriteriaBuilder().createQuery(Partner.class);
        Root<Partner> root = criteriaQuery.from(Partner.class);
        return em.createQuery(criteriaQuery.select(root)).getResultList();
    }

    @Override
    public Partner findById(Integer id) {
        return em.find(Partner.class, id);
    }

    @Override
    public Partner saveOrUpdate(Partner partner) {
        return em.merge(partner);
    }

    @Override
    public void delete(Integer id) {
        em.remove(em.find(Partner.class, id));
    }

    @Override
    public Partner findByEmail(String email) {
        CriteriaQuery<Partner> criteriaQuery = em.getCriteriaBuilder().createQuery(Partner.class);
        Root<Partner> root = criteriaQuery.from(Partner.class);
        criteriaQuery.select(root);
        criteriaQuery.where(em.getCriteriaBuilder().equal(root.get("email"), email));

        return em.createQuery(criteriaQuery).getSingleResult();
    }
}
