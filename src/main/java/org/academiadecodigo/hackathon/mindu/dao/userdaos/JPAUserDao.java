package org.academiadecodigo.hackathon.mindu.dao.userdaos;

import org.academiadecodigo.hackathon.mindu.model.users.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class JPAUserDao implements UserDao<User> {

    private EntityManager em;

    @PersistenceContext
    public void setEm(EntityManager em) {
        this.em = em;
    }

    @Override
    public List<User> findAll() {
        CriteriaQuery<User> criteriaQuery = em.getCriteriaBuilder().createQuery(User.class);
        Root<User> root = criteriaQuery.from(User.class);

        return em.createQuery(criteriaQuery.select(root)).getResultList();
    }

    @Override
    public User findById(Integer id) {
        return em.find(User.class, id);
    }

    @Override
    public User saveOrUpdate(User user) {
        return em.merge(user);
    }

    @Override
    public void delete(Integer id) {
        em.remove(em.find(User.class, id));
    }

    @Override
    public User findByEmail(String email) {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = cb.createQuery(User.class);
        Root<User> root = criteriaQuery.from(User.class);
        criteriaQuery.select(root);
        criteriaQuery.where(cb.equal(root.get("email"), email));

        return em.createQuery(criteriaQuery).getSingleResult();
    }
}
