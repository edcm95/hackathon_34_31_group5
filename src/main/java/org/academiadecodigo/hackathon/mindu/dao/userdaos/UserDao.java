package org.academiadecodigo.hackathon.mindu.dao.userdaos;

import org.academiadecodigo.hackathon.mindu.dao.Dao;
import org.academiadecodigo.hackathon.mindu.model.users.AbstractUser;

public interface UserDao<T extends AbstractUser> extends Dao<T> {

    T findByEmail(String email);
}
