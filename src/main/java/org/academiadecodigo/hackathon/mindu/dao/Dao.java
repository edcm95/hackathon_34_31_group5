package org.academiadecodigo.hackathon.mindu.dao;


import java.util.List;

public interface Dao<T> {

    List<T> findAll();
    T findById(Integer id);
    T saveOrUpdate(T model);
    void delete(Integer id);

}
