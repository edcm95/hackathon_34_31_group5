package org.academiadecodigo.hackathon.mindu.dao.userdaos;

import org.academiadecodigo.hackathon.mindu.model.users.AbstractUser;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class AbstractUserDao implements UserDao<AbstractUser> {

    private EntityManager em;

    @PersistenceContext
    public void setEm(EntityManager em) {
        this.em = em;
    }

    @Override
    public List<AbstractUser> findAll() {
        CriteriaQuery<AbstractUser> criteriaQuery = em.getCriteriaBuilder().createQuery(AbstractUser.class);
        return em.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public AbstractUser findById(Integer id) {
        return em.find(AbstractUser.class ,id);
    }

    @Override
    public AbstractUser saveOrUpdate(AbstractUser model) {
        return em.merge(model);
    }

    @Override
    public void delete(Integer id) {
        em.remove(em.find(AbstractUser.class, id));
    }

    @Override
    public AbstractUser findByEmail(String email) {
        CriteriaQuery<AbstractUser> criteriaQuery = em.getCriteriaBuilder().createQuery(AbstractUser.class);
        Root<AbstractUser> root = criteriaQuery.from(AbstractUser.class);
        criteriaQuery.select(root).where(em.getCriteriaBuilder().like(root.get("email"), email));

        return em.createQuery(criteriaQuery).getSingleResult();
    }
}
