package org.academiadecodigo.hackathon.mindu.services;

import java.util.List;

public interface ParamService<T> {

    List<T> list();
    T get(Integer id);
    T save(T object);
    void delete(Integer id);
}
