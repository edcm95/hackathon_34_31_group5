package org.academiadecodigo.hackathon.mindu.services.users;

import org.academiadecodigo.hackathon.mindu.model.users.AbstractUser;
import org.academiadecodigo.hackathon.mindu.services.ParamService;

public interface AbstractUserService<T extends AbstractUser> extends ParamService<T> {

    T findByEmail(String email);
}
