package org.academiadecodigo.hackathon.mindu.services.users;

import org.academiadecodigo.hackathon.mindu.dao.userdaos.UserDao;
import org.academiadecodigo.hackathon.mindu.errorhandling.exception.MindUException;
import org.academiadecodigo.hackathon.mindu.model.users.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class UserServiceImpl {

    private UserDao<User> userDao;

    @Autowired
    public void setUserDao(UserDao<User> userDao) {
        this.userDao = userDao;
    }

    public List<User> list() {
        return userDao.findAll();
    }

    public User get(Integer id) {
        return userDao.findById(id);
    }

    @Transactional
    public User save(User object) {
        return userDao.saveOrUpdate(object);
    }

    public void delete(Integer id) {
        userDao.delete(id);
    }

    public User findByEmail(String email) throws MindUException {
        try {
            return userDao.findByEmail(email);
        } catch (NoResultException e) {
            throw new MindUException("No result found.");
        }
    }
}
