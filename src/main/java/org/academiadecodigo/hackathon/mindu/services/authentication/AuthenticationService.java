package org.academiadecodigo.hackathon.mindu.services.authentication;

import org.academiadecodigo.hackathon.mindu.errorhandling.exception.MindUException;
import org.academiadecodigo.hackathon.mindu.model.dtos.Credentials;
import org.academiadecodigo.hackathon.mindu.model.users.AbstractUser;
import org.academiadecodigo.hackathon.mindu.services.users.PartnerServiceImpl;
import org.academiadecodigo.hackathon.mindu.services.users.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService {

    private PartnerServiceImpl partnerService;
    private UserServiceImpl userServiceImpl;

    @Autowired
    public void setPartnerServiceImpl(PartnerServiceImpl partnerService) {
        this.partnerService = partnerService;
    }

    @Autowired
    public void setUserServiceImpl(UserServiceImpl userService) {
        this.userServiceImpl = userService;
    }

    public AbstractUser authenticate(Credentials user) throws MindUException {
        AbstractUser retrieved = userServiceImpl.findByEmail(user.getEmail());

        if (retrieved == null) {
            retrieved = partnerService.findByEmail(user.getEmail());
        }

        if (retrieved == null) {
            throw new MindUException("User not found.");
        }

        if (!user.getAuthentication().equals(retrieved.getAuthentication())) {
            throw new MindUException("Credentials don't match");
        }

        return retrieved;
    }


}
