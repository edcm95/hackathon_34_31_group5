package org.academiadecodigo.hackathon.mindu.services.users;

import org.academiadecodigo.hackathon.mindu.dao.userdaos.UserDao;
import org.academiadecodigo.hackathon.mindu.errorhandling.exception.MindUException;
import org.academiadecodigo.hackathon.mindu.model.users.Partner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class PartnerServiceImpl {

    private UserDao<Partner> professionalDao;

    @Autowired
    public void setProfessionalDao(UserDao<Partner> professionalDao) {
        this.professionalDao = professionalDao;
    }

    public List<Partner> list() {
        return professionalDao.findAll();
    }

    public Partner get(Integer id) {
        return professionalDao.findById(id);
    }

    @Transactional
    public Partner save(Partner partner) {
        return professionalDao.saveOrUpdate(partner);
    }

    public void delete(Integer id) {
        professionalDao.delete(id);
    }

    public Partner findByEmail(String email) throws MindUException {
        try {

            return professionalDao.findByEmail(email);
        } catch (NoResultException e) {
            throw new MindUException("No result found");
        }
    }
}
